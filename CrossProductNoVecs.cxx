// perform a vector cross product
// hopefully this reuses enough data to be useful

// TBB and CUDA versions will set this variable if they are present
// however if no other versions exist, we fall back to serial
#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include <stdlib.h>
#include <string.h>

#include <vtkm/Math.h>
#include <vtkm/VectorAnalysis.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/WorkletMapField.h>

int numIterations = 1;
bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;

using VectorType = vtkm::Vec<vtkm::FloatDefault, 3>; //not sure?
using ArrayType  = vtkm::cont::ArrayHandle<vtkm::FloatDefault>;
using PortalType = ArrayType::PortalControl;

class VectorCrossWorklet : public vtkm::worklet::WorkletMapField {
  public:
    typedef void ControlSignature(FieldIn<vtkm::FloatDefault> ax,
				  FieldIn<vtkm::FloatDefault> ay,
				  FieldIn<vtkm::FloatDefault> az,
				  FieldIn<vtkm::FloatDefault> bx,
				  FieldIn<vtkm::FloatDefault> by,
				  FieldIn<vtkm::FloatDefault> bz,
                                  FieldOut<vtkm::FloatDefault> rx,
				  FieldOut<vtkm::FloatDefault> ry,
				  FieldOut<vtkm::FloatDefault> rz);
    typedef void ExecutionSignature(_1, _2, _3, _4, _5, _6, _7, _8, _9);
    typedef   _1 InputDomain;

    VTKM_EXEC
    void operator()(const vtkm::FloatDefault &ax,
		    const vtkm::FloatDefault &ay,
		    const vtkm::FloatDefault &az,
		    const vtkm::FloatDefault &bx,
		    const vtkm::FloatDefault &by,
		    const vtkm::FloatDefault &bz,
		    vtkm::FloatDefault &rx,
                    vtkm::FloatDefault &ry,
                    vtkm::FloatDefault &rz) const {
      rx = ay*bz - az*by;
      ry= az*bx - ax*bz;
      rz = ax*by - ay*bx;
    }
};

VTKM_CONT
void computeCrossProducts(ArrayType &vecIn1, ArrayType &vecIn2, ArrayType &vecIn3, ArrayType &vecIn4, ArrayType &vecIn5, ArrayType &vecIn6, ArrayType &vecOut1, ArrayType &vecOut2, ArrayType &vecOut3)
{
  VectorCrossWorklet worklet;
  vtkm::worklet::DispatcherMapField<VectorCrossWorklet> dispatcher(worklet);

  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    // actually run the thing
    dispatcher.Invoke(vecIn1, vecIn2, vecIn3, vecIn4, vecIn5, vecIn6, vecOut1, vecOut2, vecOut3);

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  if(checkAnswer)
    {
    PortalType port1 = vecIn1.GetPortalControl();
    PortalType port2 = vecIn2.GetPortalControl();
    PortalType port3 = vecIn3.GetPortalControl();
    PortalType port4 = vecIn4.GetPortalControl();
    PortalType port5 = vecIn5.GetPortalControl();
    PortalType port6 = vecIn6.GetPortalControl();
    PortalType port7 = vecOut1.GetPortalControl();
    PortalType port8 = vecOut2.GetPortalControl();
    PortalType port9 = vecOut3.GetPortalControl();

    for(vtkm::Id index = 0; index < port1.GetNumberOfValues(); index++)
      {
      vtkm::FloatDefault v1 = port1.Get(index);
      vtkm::FloatDefault v2 = port2.Get(index);
      vtkm::FloatDefault v3 = port3.Get(index);
      vtkm::FloatDefault v4 = port4.Get(index);
      vtkm::FloatDefault v5 = port5.Get(index);
      vtkm::FloatDefault v6 = port6.Get(index);
      vtkm::FloatDefault v7 = port7.Get(index);
      vtkm::FloatDefault v8 = port8.Get(index);
      vtkm::FloatDefault v9 = port9.Get(index);

      //vtkm::FloatDefault d = vtkm::dot(v1, v2);
      VectorType correct = vtkm::Cross(VectorType(v1,v2,v3), VectorType(v4,v5,v6));
      if(correct != VectorType(v7,v8,v9))
        {
        std::cerr << "Cross product wrong" << std::endl;
        std::cerr << "<" << v1 << ", " << v2 << ", " << v3 << "> X <" << v4 << ", " << v5 << ", " << v6 << "> was computed to be <";
        std::cerr << v7 << ", " << v8 << "," << v9 << "> but should be " << std::endl;
        std::cerr << correct[0] << ", " << correct[1] << "," << correct[2] << std::endl;
        return;
        }
    }
  }

    //only save times if answers check out
  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }
}

void populateWithUnits(vtkm::cont::ArrayHandle<vtkm::FloatDefault> &vec1,
                       vtkm::cont::ArrayHandle<vtkm::FloatDefault> &vec2,
		       vtkm::cont::ArrayHandle<vtkm::FloatDefault> &vec3,
		       vtkm::cont::ArrayHandle<vtkm::FloatDefault> &vec4,
		       vtkm::cont::ArrayHandle<vtkm::FloatDefault> &vec5,
		       vtkm::cont::ArrayHandle<vtkm::FloatDefault> &vec6)
{
  PortalType port1 = vec1.GetPortalControl();
  PortalType port2 = vec2.GetPortalControl();
  PortalType port3 = vec3.GetPortalControl();
  PortalType port4 = vec4.GetPortalControl();
  PortalType port5 = vec5.GetPortalControl();
  PortalType port6 = vec6.GetPortalControl();
  for(vtkm::Id index = 0; index < port1.GetNumberOfValues(); index++)
  {
    float r1 = (((float) rand()) / (float) RAND_MAX)*10.0; //[0 to 10)
    float r2 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r3 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r4 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r5 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r6 = (((float) rand()) / (float) RAND_MAX)*10.0;

    port1.Set(index, r1);
    port2.Set(index, r2);
    port3.Set(index, r3);
    port4.Set(index, r4);
    port5.Set(index, r5);
    port6.Set(index, r6);
  }
}

void printUsage()
{
  std::cerr << "Usage: CrossProductNoVecs_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Cross Product No Vectors" << std::endl;

  ArrayType vectorsIn1;
  ArrayType vectorsIn2;
  ArrayType vectorsIn3;
  ArrayType vectorsIn4;
  ArrayType vectorsIn5;
  ArrayType vectorsIn6;
  ArrayType vectorsOut1;
  ArrayType vectorsOut2;
  ArrayType vectorsOut3;
  const vtkm::Id ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
  vectorsIn1.Allocate(ARRAY_LENGTH);
  vectorsIn2.Allocate(ARRAY_LENGTH);
  vectorsIn3.Allocate(ARRAY_LENGTH);
  vectorsIn4.Allocate(ARRAY_LENGTH);
  vectorsIn5.Allocate(ARRAY_LENGTH);
  vectorsIn6.Allocate(ARRAY_LENGTH);
  vectorsOut1.Allocate(ARRAY_LENGTH);
  vectorsOut2.Allocate(ARRAY_LENGTH);
  vectorsOut3.Allocate(ARRAY_LENGTH);

  // fill with <1, 0, 0> and <0, 1, 0>
  populateWithUnits(vectorsIn1, vectorsIn2, vectorsIn3, vectorsIn4, vectorsIn5, vectorsIn6);

  computeCrossProducts(vectorsIn1, vectorsIn2, vectorsIn3, vectorsIn4, vectorsIn5, vectorsIn6,
                       vectorsOut1, vectorsOut2, vectorsOut3);

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        VectorType(-SIDE_LENGTH/2.0, -SIDE_LENGTH/2.0, 0), // center x&y dims
        VectorType(1.0, 1.0, 1.0));

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "crossProductx", vectorsOut1); //not sure on this
    dataSetFieldAdd.AddPointField(dataSet, "crossProducty", vectorsOut2);
    dataSetFieldAdd.AddPointField(dataSet, "crossProductz", vectorsOut3);

    vtkm::io::writer::VTKDataSetWriter writer("crossProduct.vtk");
    writer.WriteDataSet(dataSet);
  }

  return 0;
}


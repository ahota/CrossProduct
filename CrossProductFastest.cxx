#include <iostream>
#include <chrono>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <numeric>
#include <math.h>

int numIterations = 1;
//bool writeToFile = false,
bool saveTimes = false, checkAnswer = false;
int SIDE_LENGTH = 256;
std::string fileSuffix;

void cross(float* ax, float* ay, float* az,
           float* bx, float* by, float* bz, float* rx, float* ry, float* rz, int len)
{
  for (int i = 0; i <len; ++i)
    {
    rx[i] = ay[i]*bz[i] - az[i]*by[i];
    ry[i] = az[i]*bx[i] - ax[i]*bz[i];
    rz[i] = ax[i]*by[i] - ay[i]*bx[i];
    }
}

void populateWithUnits(float* a, float* b, float* c,
                       float* d, float* e, float* f, int len)
{
  for(int index = 0; index < len; index++)
  {
    a[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    b[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    c[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    d[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    e[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
    f[index] = (((float) rand()) / (float) RAND_MAX)*10.0;
  }
}

void printUsage()
{
  std::cerr << "Usage: DotProductTest ["; //-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
  /*if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
      }
      else*/
  if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
  else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Cross Product Fastest" << std::endl;

  const int ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;

  float* ax = new float[ARRAY_LENGTH];  //seems to be .021ms faster (for length 60^3) from the stack (float a[ARRAY_LENGTH]) but this means -l can be at most 60
  float* ay = new float[ARRAY_LENGTH];
  float* az = new float[ARRAY_LENGTH];
  float* bx = new float[ARRAY_LENGTH];
  float* by = new float[ARRAY_LENGTH];
  float* bz = new float[ARRAY_LENGTH];
  float* rx = new float[ARRAY_LENGTH];
  float* ry = new float[ARRAY_LENGTH];
  float* rz = new float[ARRAY_LENGTH];

  populateWithUnits(ax,ay,az,bx,by,bz, ARRAY_LENGTH);

  int total = 0;
  int times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(int index = 0; index < numIterations; index++)
    {
    if(index > 0)
      std::cout << "\r";
    auto st = std::chrono::high_resolution_clock::now();
    cross(ax, ay, az, bx, by, bz, rx, ry, rz, ARRAY_LENGTH);
    auto et = std::chrono::high_resolution_clock::now();

    int run = std::chrono::duration_cast<std::chrono::microseconds>(et-st).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
    }
  std::cout << std::endl;

  if(checkAnswer)
    {
    for(int index = 0; index < ARRAY_LENGTH; index++)
      {
      float cx = ay[index] * bz[index] - by[index] * az[index]; //verified correct
      float cy = bx[index] * az[index] - ax[index] * bz[index];
      float cz = ax[index] * by[index] - bx[index] * ay[index];

      if(fabs(rx[index]-cx) > .0001 ||
         fabs(ry[index]-cy) > .0001 ||
         fabs(rz[index]-cz) > .0001)
        {
        std::cerr << "Cross product wrong" << std::endl;
        std::cerr << "<" << ax[index] << ", " << ay[index] << ", " << az[index] << "> cross ";
        std::cerr << "<" << bx[index] << ", " << by[index] << ", " << bz[index] << "> = ";
        std::cerr << rx[index] << ", " << ry[index] << ", " << rz[index] << " but should equal " << cx << ", " << cy << ", " << cz << std::endl;
        return 1;
        }
      }
   }

  if(saveTimes)
    {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(int index = 0; index < numIterations; index++)
      {
      outfile << times[index] << '\n';
      }
    }

  return 0;
}

// perform a vector cross product
// hopefully this reuses enough data to be useful

// TBB and CUDA versions will set this variable if they are present
// however if no other versions exist, we fall back to serial
#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include <stdlib.h>
#include <string.h>

#include <vtkm/Math.h>
#include <vtkm/VectorAnalysis.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/WorkletMapField.h>

int numIterations = 1;
bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;

using VectorType = vtkm::Vec<vtkm::FloatDefault, 3>;
using ArrayType  = vtkm::cont::ArrayHandle<VectorType>;
using PortalType = ArrayType::PortalControl;

class VectorCrossWorklet : public vtkm::worklet::WorkletMapField {
  public:
    typedef void ControlSignature(FieldIn<Vec3> vectorA,
                                  FieldIn<Vec3> vectorB,
                                  FieldOut<Vec3> cross);
    typedef void ExecutionSignature(_1, _2, _3);
    typedef   _1 InputDomain;

    VTKM_EXEC
    void operator()(const VectorType &vectorA,
                    const VectorType &vectorB,
                          VectorType &cross) const {
      //cross = vtkm::Cross(vectorA, vectorB);
      vtkm::FloatDefault s1 = vectorA[1]*vectorB[2] - vectorA[2]*vectorB[1];
      vtkm::FloatDefault s2 = vectorA[2]*vectorB[0] - vectorA[0]*vectorB[2];
      vtkm::FloatDefault s3 = vectorA[0]*vectorB[1] - vectorA[1]*vectorB[0];
      cross = VectorType(s1, s2, s3);
    }
};

VTKM_CONT
void computeCrossProducts(ArrayType &vecIn1, ArrayType &vecIn2, ArrayType &vecOut)
{
  VectorCrossWorklet worklet;
  vtkm::worklet::DispatcherMapField<VectorCrossWorklet> dispatcher(worklet);

  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    // actually run the thing
    dispatcher.Invoke(vecIn1, vecIn2, vecOut);

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  if(checkAnswer) {
    PortalType port1 = vecIn1.GetPortalControl();
    PortalType port2 = vecIn2.GetPortalControl();
    PortalType port3 = vecOut.GetPortalControl();
    for(vtkm::Id index = 0; index < port1.GetNumberOfValues(); index++) {
      VectorType v1 = port1.Get(index);
      VectorType v2 = port2.Get(index);
      VectorType v3 = port3.Get(index);
      VectorType correct = vtkm::Cross(v1,v2);
      if(correct != v3)
        {
        std::cerr << "Cross Product Wrong" << std::endl;
        std::cerr << "<" << v1[0] << ", " << v1[1] << ", " << v1[2] << "> X <" << v2[0] << ", " << v2[1] << ", " << v2[2] << "> was calculated to be ";
        std::cerr << "<" << v3[0] << ", " << v3[1] << ", " << v3[2] << "> but should instead be ";
        std::cerr << "<" << correct[0] << ", " << correct[1] << ", " << correct[2] << ">" << std::endl;
        return;
        }
    }
  }

  //only save times if answers check out
  if(saveTimes) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }
}

void populateWithUnits(vtkm::cont::ArrayHandle<VectorType> &vec1,
                       vtkm::cont::ArrayHandle<VectorType> &vec2)
{
  PortalType port1 = vec1.GetPortalControl();
  PortalType port2 = vec2.GetPortalControl();
  for(vtkm::Id index = 0; index < port1.GetNumberOfValues(); index++)
  {
    float r1 = (((float) rand()) / (float) RAND_MAX)*10.0; //[0 to 10)
    float r2 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r3 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r4 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r5 = (((float) rand()) / (float) RAND_MAX)*10.0;
    float r6 = (((float) rand()) / (float) RAND_MAX)*10.0;
    port1.Set(index, VectorType(r1, r2, r3));
    port2.Set(index, VectorType(r4, r5, r6));
  }
}

void printUsage()
{
  std::cerr << "Usage: CrossProduct_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Cross Product" << std::endl;

  ArrayType vectorsIn1;
  ArrayType vectorsIn2;
  ArrayType vectorsOut;
  const vtkm::Id ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
  vectorsIn1.Allocate(ARRAY_LENGTH);
  vectorsIn2.Allocate(ARRAY_LENGTH);
  vectorsOut.Allocate(ARRAY_LENGTH);

  // fill with <1, 0, 0> and <0, 1, 0>
  populateWithUnits(vectorsIn1, vectorsIn2);

  computeCrossProducts(vectorsIn1, vectorsIn2, vectorsOut);

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        VectorType(-SIDE_LENGTH/2.0, -SIDE_LENGTH/2.0, 0), // center x&y dims
        VectorType(1.0, 1.0, 1.0));

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "crossProduct", vectorsOut);

    vtkm::io::writer::VTKDataSetWriter writer("crossProduct.vtk");
    writer.WriteDataSet(dataSet);
  }

  return 0;
}

